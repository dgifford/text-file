# Text File Edit #

Class for manipulating text files line by line.

Designed for editing/ creating text files where sections or lines are easily distinguished by the lines around them.

For example, configuration files like .htaccess where sections may be defined by comments or if statements.


## Instantiation ##

The object can be initiated by providing the constructor with:

* A valid file path
* A string
* An array of strings (lines)

```
$text = new TextFile( 'foo.txt' );

$text = new TextFile( "line 1\nline 2\nline 3\nline 4" );

$text = new TextFile( ["line 1", "line 2", "line 3", "line 4"] );
```

Or, by using the load method:

```
$text = new TextFile();

$text->load( 'foo.txt' );

$text->load( "line 1\nline 2\nline 3\nline 4" );

$text->load( ["line 1", "line 2", "line 3", "line 4"] );
```

## Array Access ##

The class implements array access and array iterator, so lines can be accessed using array notation and a foreach loop:
```
$text->load( 'foo.txt' );

// Output the first line
echo $text[0];

// Iterate over each line
foreach( $text as $index => $line )
{
	...
}
```

## Editing ##

Lines can be edited directly using array access:

```
$text->load( 'foo.txt' );

// Change the first line
echo $text[0] = 'This is the first line';

// Delete the first line
unset( $text[0] );

```

The following methods provide more flexibility when editing.

### delete ###

Deletes all lines: `$text->delete()`

Delete all lines containing the string 'bar': `$text->delete( 'bar' )`

Deleting blocks of lines can be achieved by providing two strings to the method to identify the start and end lines.

However, lines will only be deleted if the section is unique (start and end lines only occur once) and start line is before the end line.

For example, to delete the block of lines where the first line contains 'foo' and the last line contains 'bar':

```
$result = $text->delete( 'bar', 'foo' );
```

If the block was successfully deleted, `$result` will be `true` otherwise it will be `false`.

Adding a third boolean parameter will make the block exclusive, i.e. not contain the start and end lines:

```
$result = $text->delete( 'bar', 'foo', false ); 
```

### replace ###

The parameters for the replace method are similar to those for delete, except that replacement line(s) are included.

```
$text->replace( 'foo', 'replace', 'bar');
```

This replaces the block where the first line contains 'bar' and the last line contains 'foo', with the line 'replace'.

Again, lines will only be replaced if the block is unique and start line is before the end line. It returns false if the block is invalid.

Add `false` as a fourth parameter to replace the block but exclude the start and end lines:

```
$text->replace( 'foo', 'replace', 'bar', false );
```

This method also accepts an array of lines as replacements:

```
$text->replace( 'foo', ['replace', 'replace', 'replace'], 'bar', false );
```



### find ###

The `find` returns the indexes of lines containing a search string. It is used internally by methods such as `delete` and `replace`.

To find all lines containing 'foo': `$result = $text->find( 'foo' )`

The result will be an array of indexes, or false if no lines found.

This method can also be used to find unique lines by adding a second `false` parameter:

```
$result = $text->find( 'foo', false ); 
```

The result will be an integer, or `false` if no line found or more than one line is found.

### prepend ###

This method adds a single line containing 'foo' to the start of the file:  `$text->prepend( 'foo' )`

Add multiple lines to the start of the file: `$text->prepend( ['foo', 'foo', 'foo'] )`

### append ###

Add a single line containing 'foo' to the end of the file: `$text->append( 'foo' )`

Add multiple lines to the end of the file: `$text->append( ['foo', 'foo', 'foo'] )`

### add ###

This method adds lines before a specified line.

To add a line containing 'bar' before the line(s) containing 'foo': `$text->add( 'foo', 'bar' )`

To add multiple lines before the line(s) containing 'foo': `$text->add( 'foo', ['bar', 'bar', 'bar'] )`


## Extracting lines ###

The result of extracting lines is a new TextFile object containing the extracted lines.

As with other methods, a block of lines to extract is identified by strings in the start line and end lines.

This will extract the lines between, and including, the line containing 'bar' and the line containing 'foo':

```
$result = $text->extract( 'foo', 'bar' );
```

Adding `false` as a third parameter will exclude the start and end lines: `$result = $text->extract( 'foo', 'bar', false)`

Leaving out the second parameter will extract all lines from the first to the end of the file, including the first line.

```
$result = $text->extract( 'foo' );
```

## Output ##

The `join` method returns the lines as a string with an end of line character between each line.

By default, the PHP constant `PHP_EOL` is used, but it can be set by: `$text->setEOL( "\n" )`

To echo the contents of the file as a string: `echo $text->join()`

To save the file `$text->save( 'foo.txt' )`