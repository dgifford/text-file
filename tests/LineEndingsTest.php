<?php
Namespace dgifford\TextFile;



Use dgifford\TextFile\TextFile;

/**
 * Auto Loader
 * 
 */
require_once(__DIR__ . '/../vendor/autoload.php');



class LineEndingsTest extends \PHPUnit_Framework_TestCase
{
	public function testForUnixEol()
	{
		$text = new TextFile();

		$this->assertSame( "\n", $text->findEOL( "line 1\nline 2\nline 3\nline 4" ) );
	}



	public function testForWindowsEol()
	{
		$text = new TextFile();

		$this->assertSame( "\r\n", $text->findEOL( "line 1\r\nline 2\r\nline 3\r\nline 4" ) );
	}



	public function testForOldMacEol()
	{
		$text = new TextFile();

		$this->assertSame( "\r", $text->findEOL( "line 1\rline 2\rline 3\rline 4" ) );
	}



	public function testForMixedEol()
	{
		$text = new TextFile();

		$this->assertFalse( $text->findEOL( "line 1\rline 2\r\nline 3\nline 4" ) );
	}



	public function testMixedEolConverted()
	{
		$text = new TextFile( "line 1\rline 2\r\nline 3\nline 4" );

		$this->assertTrue( $text->isChanged() );
		$this->assertSame( "line 1" . $text->getEOL() . "line 2" . $text->getEOL() . "line 3" . $text->getEOL() . "line 4", $text->join() );
	}
}