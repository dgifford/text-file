<?php
Namespace dgifford\TextFile;



Use dgifford\TextFile\TextFile;

/**
 * Auto Loader
 * 
 */
require_once(__DIR__ . '/../vendor/autoload.php');



class TextFileTest extends \PHPUnit_Framework_TestCase
{
	public function setUp()
	{
		$this->text = new TextFile( __DIR__ . '/test.txt'  );
	}



	public function testLoadFileInConstructor()
	{
		$this->assertSame( 'line 1'  . $this->text->getEOL() . 'line 2'  . $this->text->getEOL() . 'line 3'  . $this->text->getEOL() . 'line 4'  . $this->text->getEOL() . 'line 5'  . $this->text->getEOL() . 'line 6'  . $this->text->getEOL() . 'line 7'  . $this->text->getEOL() . 'line 8', $this->text->join() );
	}



    public function testEmptyConstructor()
	{
		$text = new TextFile( false );

		$this->assertSame( '', $text->join() );
	}



	public function testConvertMixedLineEndings()
	{
		$text = new TextFile( "line 1\rline 2\r\nline 3\nline 4" );

		$this->assertSame( "line 1" . $text->getEOL() . "line 2" . $text->getEOL() . "line 3" . $text->getEOL() . "line 4", $text->join() );
	}



	public function testChangedReturnsFalse()
	{
		$text = new TextFile( "line 1" . PHP_EOL . "line 2" . PHP_EOL . "line 3" . PHP_EOL . "line 4" );

		$this->assertFalse( $text->isChanged() );
	}



	public function testReplaceInclusive()
	{
		$this->text->replace( 'line 1', 'replace', 'line 3');

		$this->assertSame( "replace" . $this->text->getEOL() . "line 4" . $this->text->getEOL() . "line 5" . $this->text->getEOL() . "line 6" . $this->text->getEOL() . "line 7". $this->text->getEOL() . "line 8", $this->text->join() );
	}



	public function testReplaceNotInclusive()
	{
		$this->text->replace( 'line 1', 'replace', 'line 3', false);

		$this->assertSame( "line 1" . $this->text->getEOL() . "replace" . $this->text->getEOL() . "line 3" . $this->text->getEOL() . "line 4" . $this->text->getEOL() . "line 5" . $this->text->getEOL() . "line 6" . $this->text->getEOL() . "line 7". $this->text->getEOL() . "line 8", $this->text->join() );
	}



	public function testReplaceWithAnArray()
	{
		$this->text->replace( 'line 1', ['replace', 'replace', 'replace'], 'line 3', false);

		$this->assertSame( "line 1" . $this->text->getEOL() . "replace" . $this->text->getEOL() . "replace" . $this->text->getEOL() . "replace" . $this->text->getEOL() . "line 3" . $this->text->getEOL() . "line 4" . $this->text->getEOL() . "line 5" . $this->text->getEOL() . "line 6" . $this->text->getEOL() . "line 7". $this->text->getEOL() . "line 8", $this->text->join() );
	}



	public function testReplaceWithStartAndEndIdentical()
	{
		$this->text->replace( 'line 1', 'replace', 'line 1' );

		$this->assertSame( "replace" . $this->text->getEOL() . "line 2" . $this->text->getEOL() . "line 3" . $this->text->getEOL() . "line 4" . $this->text->getEOL() . "line 5" . $this->text->getEOL() . "line 6" . $this->text->getEOL() . "line 7". $this->text->getEOL() . "line 8", $this->text->join() );
	}



	public function testFindMultiple()
	{
		$this->assertSame( [0, 1, 2, 3, 4, 5, 6, 7 ], $this->text->find( 'line' ) );
	}



	public function testFindExpectSingleMultipleNotAllowed()
	{
		$this->assertSame( 5, $this->text->find( '6', false ) );
	}



	public function testFindExpectMultipleButMultipleNotAllowed()
	{
		$this->assertFalse( $this->text->find( 'line', false ) );
	}



	public function testExtractInclusive()
	{
		$result = $this->text->extract( 'line 1', 'line 3' );

		$this->assertSame( "line 1" . $this->text->getEOL() . "line 2" . $this->text->getEOL() . "line 3", $result->join() );
	}



	public function testExtractNotInclusive()
	{
		$result = $this->text->extract( 'line 1', 'line 3', false );

		$this->assertSame( "line 2", $result->join() );
	}



	public function testExtractAllLinesToEndInclusive()
	{
		$result = $this->text->extract( 'line 4' );

		$this->assertSame( "line 4" . $this->text->getEOL() . "line 5" . $this->text->getEOL() . "line 6" . $this->text->getEOL() . "line 7". $this->text->getEOL() . "line 8",  $result->join() );
	}



	public function testExtractAllLinesToEndNotInclusive()
	{
		$result = $this->text->extract( 'line 4', '', false );

		$this->assertSame( "line 5" . $this->text->getEOL() . "line 6" . $this->text->getEOL() . "line 7". $this->text->getEOL() . "line 8", $result->join() );
	}



	public function testCount()
	{
		$this->assertSame( 8, $this->text->count() );
	}



	public function testAddToStart()
	{
		$this->text->add( 'line 1', 'add' );

		$this->assertSame( "add" . $this->text->getEOL() . "line 1" . $this->text->getEOL() . "line 2" . $this->text->getEOL() . "line 3" . $this->text->getEOL() . "line 4" . $this->text->getEOL() . "line 5" . $this->text->getEOL() . "line 6" . $this->text->getEOL() . "line 7". $this->text->getEOL() . "line 8", $this->text->join() );
	}



	public function testAddArrayToStart()
	{
		$this->text->add( 'line 1', ['add', 'add' ,'add' ] );

		$this->assertSame( "add" . $this->text->getEOL() . "add" . $this->text->getEOL() . "add" . $this->text->getEOL() . "line 1" . $this->text->getEOL() . "line 2" . $this->text->getEOL() . "line 3" . $this->text->getEOL() . "line 4" . $this->text->getEOL() . "line 5" . $this->text->getEOL() . "line 6" . $this->text->getEOL() . "line 7". $this->text->getEOL() . "line 8", $this->text->join() );
	}



	public function testAddBeforeLastLine()
	{
		$this->text->add( 'line 8', 'add' );

		$this->assertSame( "line 1" . $this->text->getEOL() . "line 2" . $this->text->getEOL() . "line 3" . $this->text->getEOL() . "line 4" . $this->text->getEOL() . "line 5" . $this->text->getEOL() . "line 6" . $this->text->getEOL() . "line 7" . $this->text->getEOL() . "add". $this->text->getEOL() . "line 8", $this->text->join() );
	}



	public function testAddToMultipleLines()
	{
		$this->text->add( 'line', 'add' );

		$this->assertSame( "add" . $this->text->getEOL() . "line 1" . $this->text->getEOL() . "add" . $this->text->getEOL() . "line 2" . $this->text->getEOL() . "add" . $this->text->getEOL() . "line 3" . $this->text->getEOL() . "add" . $this->text->getEOL() . "line 4" . $this->text->getEOL() . "add" . $this->text->getEOL() . "line 5" . $this->text->getEOL() . "add" . $this->text->getEOL() . "line 6" . $this->text->getEOL() . "add" . $this->text->getEOL() . "line 7" . $this->text->getEOL() . "add" . $this->text->getEOL() . "line 8", $this->text->join() );
	}



	public function testAddMultipleLinesBeforeMultipleLines()
	{
		$text = new TextFile( __DIR__ . '/test2.txt'  );

		$text->add( 'line 1', ['add', 'add'] );

		$this->assertSame( "add" . $text->getEOL() . "add" . $text->getEOL() . "line 1" . $text->getEOL() . "line 2" . $text->getEOL() . "line 3" . $text->getEOL() . "line 4" . $text->getEOL() . "line 5" . $text->getEOL() . "line 6" . $text->getEOL() . "line 7" . $text->getEOL() . "line 8" . $text->getEOL() . "line 9" . $text->getEOL() . "add" . $text->getEOL() . "add" . $text->getEOL() . "line 10" . $text->getEOL() . "add" . $text->getEOL() . "add" . $text->getEOL() . "line 11" . $text->getEOL() . "add" . $text->getEOL() . "add" . $text->getEOL() . "line 12", $text->join() );
	}



	public function testPrepend()
	{
		$this->text->prepend( ['add', 'add' ,'add' ] );

		$this->assertSame( "add" . $this->text->getEOL() . "add" . $this->text->getEOL() . "add" . $this->text->getEOL() . "line 1" . $this->text->getEOL() . "line 2" . $this->text->getEOL() . "line 3" . $this->text->getEOL() . "line 4" . $this->text->getEOL() . "line 5" . $this->text->getEOL() . "line 6" . $this->text->getEOL() . "line 7". $this->text->getEOL() . "line 8", $this->text->join() );
	}



	public function testAppend()
	{
		$this->text->append( ['add', 'add' ,'add' ] );

		$this->assertSame( "line 1" . $this->text->getEOL() . "line 2" . $this->text->getEOL() . "line 3" . $this->text->getEOL() . "line 4" . $this->text->getEOL() . "line 5" . $this->text->getEOL() . "line 6" . $this->text->getEOL() . "line 7". $this->text->getEOL() . "line 8" . $this->text->getEOL() . "add" . $this->text->getEOL() . "add" . $this->text->getEOL() . "add", $this->text->join() );
	}



	public function testDeleteAll()
	{
		$result = $this->text->delete();

		$this->assertTrue( $result );

		$this->assertTrue( $this->text->isEmpty() );
	}



	public function testDeleteOneLine()
	{
		$result = $this->text->delete( 'line 6' );

		$this->assertTrue( $result );

		$this->assertSame( "line 1" . $this->text->getEOL() . "line 2" . $this->text->getEOL() . "line 3" . $this->text->getEOL() . "line 4" . $this->text->getEOL() . "line 5" . $this->text->getEOL() . "line 7". $this->text->getEOL() . "line 8", $this->text->join() );
	}



	public function testDeleteAllMatching()
	{
		$text = new TextFile( __DIR__ . '/test2.txt'  );

		$result = $text->delete( 'line 1' );

		//var_export($this->text->join());

		$this->assertTrue( $result );

		$this->assertSame( "line 2" . $text->getEOL() . "line 3" . $text->getEOL() . "line 4" . $text->getEOL() . "line 5" . $text->getEOL() . "line 6" . $text->getEOL() . "line 7". $text->getEOL() . "line 8". $text->getEOL() . "line 9", $text->join() );
	}



	public function testDeleteBetweenTwoLinesNotInclusive()
	{
		$result = $this->text->delete( 'line 1', 'line 4', false );

		$this->assertTrue( $result );

		$this->assertSame( "line 1" . $this->text->getEOL() . "line 4" . $this->text->getEOL() . "line 5" . $this->text->getEOL() . "line 6" . $this->text->getEOL() . "line 7". $this->text->getEOL() . "line 8", $this->text->join() );
	}



	public function testDeleteBetweenTwoLinesInclusive()
	{
		$result = $this->text->delete( 'line 1', 'line 4' );

		$this->assertTrue( $result );

		$this->assertSame( "line 5" . $this->text->getEOL() . "line 6" . $this->text->getEOL() . "line 7". $this->text->getEOL() . "line 8", $this->text->join() );
	}



	public function testDeleteWithIncorrectArguments()
	{
		$result = $this->text->delete( 'line 1', true );

		$this->assertFalse( $result );
	}



	public function testDeleteWithEndBeforeStart()
	{
		$result = $this->text->delete( 'line 6', 'line 1', true );

		$this->assertFalse( $result );
	}



	public function testSave()
	{
		$this->text->save( __DIR__ . '/test-saved.txt' );

		$this->assertSame( file_get_contents(__DIR__ . '/test-saved.txt'), file_get_contents(__DIR__ . '/test.txt' ) );
	}



	public function testFindLine()
	{
		$text = new TextFile( __DIR__ . '/test3.txt'  );

		$this->assertSame( 5, $text->findLine( 'line 6', false ) );

		$this->assertSame( [5], $text->findLine( 'line 6', true ) );

		$this->assertSame( [8,9], $text->findLine( "another line", true ) );

		$this->assertFalse( $text->findLine( "another line", false ) );
	}



	public function testExtractExactMatch()
	{
		$text = new TextFile( __DIR__ . '/test3.txt'  );

		$this->assertSame( null, $text->extract( 'line 8', 'another line', false, true ) );

		$extracted = $text->extract( 'line 8', 'wibble arse', false, true );

		$this->assertSame( "another line" . $text->getEOL() . "another line", $extracted->join() );
	}



	public function testStartsWith()
	{
		$text = new TextFile( __DIR__ . '/test3.txt'  );

		$this->assertSame( [ 8, 9 ], $text->startsWith( 'an' ) );
	}



	public function testStartsWithInvalidString()
	{
		$text = new TextFile( __DIR__ . '/test3.txt'  );

		$this->assertSame( [], $text->startsWith( 'ansdfsdfsdfsdfsdfsdfsdf' ) );
	}



	public function testEndsWith()
	{
		$text = new TextFile( __DIR__ . '/test3.txt'  );

		$this->assertSame( [ 8, 9, 11 ], $text->endsWith( 'line' ) );
	}



	public function testTrimLines()
	{
		$text = new TextFile("line 1
			line 2			
			line 3  ");

		$this->assertSame( "line 1{$text->getEOL()}line 2{$text->getEOL()}line 3", $text->trimLines()->join() );
	}



	public function testTrimLinesMaskLeftSide()
	{
		$text = new TextFile;

		$text->load("line 1{$text->getEOL()}XXXline 2XXX{$text->getEOL()}XXXline 3XXX");

		$this->assertSame( "line 1{$text->getEOL()}line 2XXX{$text->getEOL()}line 3XXX", $text->trimLines( 'X', true, false )->join() );
	}



	public function testTrimLinesMaskRightSide()
	{
		$text = new TextFile;

		$text->load("line 1{$text->getEOL()}XXXline 2XXX{$text->getEOL()}XXXline 3XXX");

		$this->assertSame( "line 1{$text->getEOL()}XXXline 2{$text->getEOL()}XXXline 3", $text->trimLines( 'X', false, true )->join() );
	}



	public function testTrimLinesSpaceRightSide()
	{
		$text = new TextFile;

		$text->load("line 1		  {$text->getEOL()} 		line 2{$text->getEOL()}  	line 3  	");

		$this->assertSame( "line 1{$text->getEOL()} 		line 2{$text->getEOL()}  	line 3", $text->trimLines( '', false, true )->join() );
	}



	public function testMergeLines()
	{
		$this->assertSame( "line 1{$this->text->getEOL()}line 2{$this->text->getEOL()}line 3{$this->text->getEOL()}line 4{$this->text->getEOL()}line 5{$this->text->getEOL()}line 6line 7line 8", $this->text->merge([ 5, 6, 7, ])->join() );
	}



	public function testMergeSeperatedLines()
	{
		$this->assertSame( "line 1line 6line 7line 8{$this->text->getEOL()}line 2{$this->text->getEOL()}line 3{$this->text->getEOL()}line 4{$this->text->getEOL()}line 5", $this->text->merge([ 0, 5, 6, 7, ])->join() );
	}

}