<?php
Namespace dgifford\TextFile;

/*
	Class for manipulating text files.

	Copyright (C) 2016  Dan Gifford

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */



class TextFile implements \ArrayAccess, \Iterator
{
	Use \dgifford\Traits\ArrayAccessTrait;
	Use \dgifford\Traits\IteratorTrait;
	Use \dgifford\Traits\PropertySetterTrait;



	// Whether to trim loaded lines
	public $trim_loaded_lines = false;

	// Whether to convert end of line characters;
	public $convert_eol_character = true;

	// End of line character used
	public $eol_character;

	// Original string
	protected $original = '';

	// Container and position for array access and iterator
	protected $container = [];
	protected $position = 0;






	//////////////////////////////////////////////////////////
	// Loading/ initiating the class
	//////////////////////////////////////////////////////////
	


	/**
	 * Creates the object using the provided string/ path to a text file.
	 * 
	 * @param string $path [description]
	 */
	public function __construct( $content = '' )
	{
		if( !empty( $content ) )
		{
			$this->load( $content );
		}
	}



	/**
	 * Loads text (either s tring or from a file if path provided), splits it into seperate lines and removes EOL character.
	 * 
	 * @param  string $string  	Path to file or string
	 */
	public function load( $string = '' )
	{
		// Load an array directly
		if( is_array( $string ) )
		{
			$this->container = $string;
			$this->original = $string;
		}
		// Convert EOL chars and split string into lines
		elseif( is_string( $string ) and !empty( $string ))
		{
			// Load string from file if existing path provided
			if( $result = realpath( $string ) )
			{
				$string = file_get_contents( $string );
			}

			$this->original = $string;

			$eol =  $this->findEOL( $string );

			if( $eol === false )
			{
				$this->setEOL( PHP_EOL );
				$string = $this->convertEOL( $string, PHP_EOL );
			}
			else
			{
				$this->setEOL( $eol );
			}			

			$this->container = explode( $this->getEOL(), $string );
		}
		else
		{
			throw new \InvalidArgumentException( 'Invalid string or path.' );
		}

		// Trim lines if required
		if( $this->trim_loaded_lines )
		{
			$this->trimLines();
		}	

		$this->rewind();
	}



	protected function convertEOL( $string = '', $eol = null )
	{
		if( !empty( $string ) and !is_null( $eol ) )
		{
			return str_replace(["\r\n", "\n", "\r"], $eol, $string);
		}
	}






	//////////////////////////////////////////////////////////
	// Information about the text
	//////////////////////////////////////////////////////////
	


	/**
	 * Boolean test for an empty object.
	 * 
	 * @return boolean
	 */
	public function isEmpty()
	{
		if( empty( $this->container ) )
		{
			return true;
		}

		return false;
	}



	/**
	 * Returns true if the joined lines are different from the original text.
	 * 
	 * @return boolean
	 */
	public function isChanged()
	{
		if( $this->join() === $this->original )
		{
			return false;
		}

		return true;
	}



	public function getEOL()
	{
		if( isset( $this->eol_character ) )
		{
			return $this->eol_character;
		}

		return PHP_EOL;
	}



	public function setEOL( $eol = null )
	{
		if( !empty( $eol ) )
		{
			 $this->eol_character = $eol;
		}
		else
		{
			$this->eol_character = PHP_EOL;
		}
	}



	public function findEOL( $string = '' )
	{
		$newlines = preg_replace('/(*ANYCRLF)./', '', $string);

		if( str_replace("\r\n", '', $newlines) == '' )
		{
			return "\r\n";
		}

		if( str_replace("\r", '', $newlines) == '' )
		{
			return "\r";
		}

		if( str_replace("\n", '', $newlines) == '' )
		{
			return "\n";
		}

		return false;
	}



	public function setTrimLoadedLines( $trim_loaded_lines = true )
	{
		$this->trim_loaded_lines = boolval( $trim_loaded_lines );
	}





	//////////////////////////////////////////////////////////
	// Outputting the text
	//////////////////////////////////////////////////////////



	/**
	 * Saves to given path if the save directory exists.
	 * 
	 * @param  string $path  Save path
	 */
	public function save( $path = '' )
	{
		if( empty( $path ) or realpath( pathinfo( $path, PATHINFO_DIRNAME) ) === false )
		{
			throw new \InvalidArgumentException( 'Invalid save path.' );
		}
		else
		{
			file_put_contents( $path, $this->join() );
		}
	}



	/**
	 * Join together the lines of text and return the string.
	 * 
	 * @return string
	 */
	public function join()
	{
		return implode( $this->getEOL(), $this->container );
	}



	/**
	 * Returns a count of the lines in the text.
	 * 
	 * @return [type] [description]
	 */
	public function count()
	{
		return count( $this->container );
	}



	public function firstLine()
	{
		return $this->container[ 0 ];
	}



	public function lastLine()
	{
		return $this->container[ count($this->container) - 1 ];
	}






	//////////////////////////////////////////////////////////
	// Editing the text
	//////////////////////////////////////////////////////////



	/**
	 * Trims whitespace or a given mask from all lines.
	 * 
	 * @return null
	 */
	public function trimLines( $mask = '', $left = true, $right = true )
	{
		if( is_null($mask) or strlen( $mask ) == 0 )
		{
			// Default PHP mask
			$mask = " \t\n\r\0\x0B";
		}

		foreach( $this->container as &$line )
		{
			if( $left and $right )
			{
				$line = trim( $line, $mask );
			}
			elseif( $left and !$right )
			{
				$line = ltrim( $line, $mask );
			}
			elseif( !$left and $right )
			{
				$line = rtrim( $line, $mask );
			}
		}

		return $this;
	}



	/**
	 * Flexible method for removing lines.
	 * 	No arguments - delete all
	 * 	Provide 1 string arg to delete all lines that match 
	 * 	Provide 2 string args to delete all lines between them inclusively, provided they each only match one line
	 * 	Provide 2 string args followed by false to delete all lines between them exclusively, provided they each only match one line
	 * 
	 * @param  string $start 	Line to match or start line
	 * @param  string $end   	End line
	 * @return bool        		True if arguments valid and deletion attempted
	 */
	public function delete( $start = '', $end = '', $inclusive = true )
	{
		// Empty - delete all
		if( func_num_args() == 0 )
		{
			$this->container = [];
			return true;
		}

		// One string argument - delete all that match
		if( func_num_args() == 1 and is_string( $start ) )
		{
			$indexes = $this->find( $start, true );

			if( is_array($indexes) and !empty($indexes) )
			{
				$this->deleteIndexes( $indexes );
			}

			return true;
		}

		// Two string arguments - delete all between inclusively
		// Provided a single macth of each can be found.
		if( (func_num_args() == 2 or func_num_args() == 3 ) and is_string( $start ) and is_string( $end ) )
		{
			$start_index = $this->find( $start, false );
			$end_index = $this->find( $end, false );

			if( $start_index !== false and $end_index !== false and $start_index <= $end_index )
			{
				$indexes = range($start_index, $end_index);

				if( $inclusive === false )
				{
					array_pop( $indexes );
					array_shift( $indexes );
				}

				$this->deleteIndexes( $indexes );

				return true;
			}
		}

		return false;
	}



	/**
	 * Delete all the lines matching the indexes in the passed array.
	 * 
	 * @param  array  $indexes
	 * @return null
	 */
	public function deleteIndexes( $indexes = [] )
	{
		if( is_int($indexes) )
		{
			$indexes = [$indexes];
		}

		if( is_array($indexes) )
		{
			foreach( $indexes as $index )
			{
				if( isset($this->container[ $index ]) )
				{
					unset($this->container[ $index ]);
				}
			}

			$this->rewind();
		}
	}



	/**
	 * Returns the index of the line(s) containing the search string
	 * @param  string 	$search 			A search string
	 * @param  bool  	$multiple_allowed 	If true returns results in an array, if false returns false if > 1 result found
	 * @return int         					Line number
	 */
	public function find( $search, $multiple_allowed = true )
	{
		if( empty( $search ) )
		{
			return false;
		}

		$result = [];

		foreach( $this->container as $index => $line )
		{
			if( strpos( $line, $search ) !== false )
			{
				$result[] = $index;
			}
		}

		if( !empty( $result ) )
		{
			if( $multiple_allowed )
			{
				return $result;
			}
			elseif( count( $result ) == 1 )
			{
				return $result[0];
			}
		}

		return false;
	}



	/**
	 * Returns an array of indexes where lines start with the search string.
	 * 
	 * @param  string $search
	 * @return array
	 */
	public function startsWith( $search = '' )
	{
		$result = [];

		if( is_string($search) and !empty($search) )
		{
			foreach( $this->container as $index => $line )
			{
				if( substr($line, 0, strlen($search) ) === $search )
				{
					$result[] = $index;
				}
			}
		}
		
		return $result;
	}	



	public function endsWith( $search = '' )
	{
		$result = [];

		if( is_string($search) and !empty($search) )
		{
			foreach( $this->container as $index => $line )
			{
				if( substr($line, (-1 * strlen($search)) ) === $search )
				{
					$result[] = $index;
				}
			}
		}
		
		return $result;
	}	



	/**
	 * Returns the index of the line(s) matching the search string
	 * @param  string 	$search 			Line to search for
	 * @param  bool  	$multiple_allowed 	If true returns results in an array, if false returns false if > 1 result found
	 * @return int         					Line number
	 */
	public function findLine( $search = '', $multiple_allowed = true )
	{
		$result = [];

		foreach( $this->container as $index => $line )
		{
			if( $line === $search )
			{
				$result[] = $index;
			}
		}

		if( !empty( $result ) )
		{
			if( $multiple_allowed )
			{
				return $result;
			}
			elseif( count( $result ) == 1 )
			{
				return $result[0];
			}
		}

		return false;
	}



	/**
	 * Replaces a section defined by $start and $end with $lines
	 * @param  string 	$start 		The string used to find the starting index
	 * @param  string 	$end   		The string used to find the end index
	 * @param  string 	$lines 		The replacement lines
	 * @param  bool 	$inclusive 	Whether the $start and end lines should be included
	 */
	public function replace( $start, $lines = '', $end = '', $inclusive = true )
	{
		$start_index 	= $this->find( $start, false );
		$end_index 		= $this->find( $end, false );

		/*
			Check parameters
		 */
		if( !is_int( $start_index ) or (is_int( $end_index ) and is_int( $start_index ) and $end_index < $start_index))
		{
			return false;
		}

		$lines = $this->makeArray( $lines );

		// Single element
		if( $end_index === false or $end_index == $start_index )
		{
			if( $inclusive )
			{
				// Replace single element
				array_splice( $this->container, $start_index, 1, $lines );
			}
			else
			{
				// Insert after single element
				$this->add( $start_index, $lines );
			}
		}
		// Start and end defined
		else
		{
			if( $inclusive )
			{
				array_splice( $this->container, $start_index, ($end_index - $start_index + 1), $lines );
			}
			else
			{
				array_splice( $this->container, ($start_index + 1), ( $end_index - $start_index - 1 ), $lines );
			}
		}
	}



	/**
	 * Extracts the lines between $start and $end lines, including $start and $end lines if inclusive is true
	 * If only $start is given, all lines to end of the file are extracted.
	 * Returns a TextFile object containing the extracted lines.
	 * 
	 * @param  [type] $start [description]
	 * @param  string $end   [description]
	 * @return Object        a TextFile object
	 */
	public function extract( $start, $end = '', $inclusive = true, $exact_match = false )
	{
		if( is_bool($end) )
		{
			$inclusive = $end;
			$end = '';
		}

		if( $exact_match )
		{
			$start_index = $this->findLine( $start, false );
		}
		else
		{
			$start_index = $this->find( $start, false );
		}

		if( !empty( $end ) )
		{
			if( $exact_match )
			{
				$end_index = $this->findLine( $end, false );
			}
			else
			{
				$end_index = $this->find( $end, false );
			}			
		}
		else
		{
			$end_index = $this->count() - 1;
		}

		if( $start_index < $end_index )
		{
			if( $inclusive )
			{
				$result = array_slice( $this->container, $start_index, ($end_index - $start_index + 1) );
			}
			elseif( !$inclusive and empty( $end ) )
			{
				$result = array_slice( $this->container, ($start_index + 1), ($end_index - $start_index + 1 ) );
			}
			else
			{
				$result = array_slice( $this->container, ($start_index + 1), ($end_index - $start_index - 1 ) );
			}

			$text = new TextFile( $result );
			$text->setEOL( $this->getEOL() );

			return $text;
		}		
	}



	/**
	 * Adds a line (or array of lines) at the given line(s).
	 * 
	 * @param  string 	$line 	A line to find.
	 * @param  string 	$lines  The added line(s)
	 */
	public function add( $line = '', $lines = '' )
	{
		if( empty( $line ) or empty( $lines ) )
		{
			return;
		}

		// Find indexes
		$indexes = $this->find( $line );

		$result = [];

		if( $indexes !== false )
		{
			$lines = $this->makeArray( $lines );

			foreach( $this->container as $index => $line )
			{
				if( in_array( $index, $indexes ) )
				{
					$result = array_merge( $result, $lines );
				}

				$result[] = $line;
			}

		}

		$this->container = $result;
	}



	/**
	 * Adds a line (or array of lines) at the end of the text.
	 * 
	 * @param  string/array 	$lines  The added line(s)
	 */
	public function append( $lines = '' )
	{
		if( empty( $lines ) )
		{
			return;
		}

		$this->container = array_merge( $this->container, $this->makeArray( $lines ) );
	}



	/**
	 * Adds a line (or array of lines) at to the start of the text.
	 * 
	 * @param  string/array 	$lines  The added line(s)
	 */
	public function prepend( $lines = '' )
	{
		if( empty( $lines ) )
		{
			return;
		}

		$this->container = array_merge( $this->makeArray( $lines ), $this->container );
	}



	public function merge( $indexes = [] )
	{
		foreach( $indexes as $key => $index )
		{
			if( !isset( $this->container[ $indexes[$key] ] ) )
			{
				unset($indexes[$key]);
			}
		}

		$indexes = array_values( $indexes );

		$count = count($indexes);

		if( $count > 1)
		{
			for( $i=1; $i < $count; $i++ )
			{
				$this->container[ $indexes[0] ] .= $this->container[ $indexes[$i] ];
			}

			unset( $indexes[0] );

			$this->deleteIndexes( $indexes );
		}

		return $this;
	}






	//////////////////////////////////////////////////////////
	// Utilities
	//////////////////////////////////////////////////////////
	


	protected function makeArray( $lines = '' )
	{
		if( is_string( $lines ) )
		{
			$lines = array( $lines );
		}

		if( !is_array( $lines ) )
		{
			throw new \InvalidArgumentException( 'Invalid replacement lines for replace.' );
		}

		// Ensure numerically indexed
		$lines = array_values( $lines );

		return $lines;
	}
}